package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {


    public  static Stack<Integer> stack = new Stack<>();
    public static List<Integer> lastArr = new ArrayList<>();
    public static int max2 = 0;


    public static void main(String[] args) throws IOException {

        System.out.println("please type an array(with comma separator) : ");

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));

        String strArr = "";
        strArr = reader.readLine();

        generateStack(strArr);
        max2 = initMax();
        calculate();
        System.out.println(lastArr);
    }

    /**
     * Gelen String'i stack'e çevirir.
     * @param strArr
     */
    public static void generateStack(String strArr) {
        Arrays.stream(strArr.split(",")).forEach(x-> {
            stack.add(Integer.valueOf(x));
        });
    }

    /**
     * En büyüğü bulur
     * @return int
     */
    public static int findMaxInStack() {
        Integer[]  arr =  Arrays.stream(stack.toArray()).sorted().toArray(Integer[]::new);
        return arr[arr.length-1];
    }

    /**
     * İnitialize için başlangıç değerlerini atar.
     * @return init
     */
    public static int initMax() {
      int x =  stack.get(0);
      int y =  stack.get(1);

      if(x < y) {
          return y;
      }
      else {
          return findMax2InStack();
      }
    }

    /**
     * En büyükler arasında 2.sıradaki max'ı bulur.
     * @return int
     */
    public static int findMax2InStack() {

        Integer[]  arr =  Arrays.stream(stack.toArray()).sorted().toArray(Integer[]::new);
        return arr[arr.length-2];
    }



    public static int[] calculate() {

        for(int i=0; i< stack.size(); i++) {

            int current = stack.get(i);

            if(current < max2) {
                lastArr.add(max2-current);
            }
            else if(current > max2) {

                /*
                    Negatif sayılarda işlem yapmasın(Kendisi ve kendisinden büyük gelirse)
                 */
                if(max2- current <= 0 ) {
                }
                else {
                    lastArr.add(max2- current);
                }
            }
            else if(current == max2) {

                /*
                    Array içinde en büyük 2.ciyi bulması için gereken size
                 */
                if(stack.size() > 2) {
                    max2 = findMax2InStack();
                }
            }

            /*
                Eğer son iki tane kalmışsa en büyüğünü getir
             */
            if(stack.size() == 2) {
                max2 = findMaxInStack();
            }


            /*
             İşlemi biten stackteki int'i silip recursive şekilde çağırarak kaldığı yerden devam etmesini sağlar.
             */
            stack.remove(i);
            calculate();
        }

        return null;
    }
}
