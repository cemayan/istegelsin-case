# Kullanım

```bash
mvn clean install
java -jar target/istegelsin-case-1.0-SNAPSHOT.jar
```

# Hakkında

Verilen bir array'i bar halinde düşünürsek arasında kalan su miktarını hesaplar.

Örnek:

[7, 0, 4, 2, 5, 0, 6, 4, 0, 5 ] array'i verilsin.

Cevap: 

6,2,4,1,6,1,5